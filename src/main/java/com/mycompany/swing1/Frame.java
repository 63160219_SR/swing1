/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swing1;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author sairu
 */
public class Frame {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(new Dimension(500, 300));
        JLabel lbHelloworld = new JLabel("Hello World",JLabel.CENTER);
        lbHelloworld.setFont(new Font("Verdana",Font.PLAIN,18));
        lbHelloworld.setBackground(Color.cyan);
        lbHelloworld.setOpaque(true);
        frame.add(lbHelloworld); 
        
        frame.setVisible(true);
    }
}
