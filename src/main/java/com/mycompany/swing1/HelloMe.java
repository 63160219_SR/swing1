/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swing1;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

class MyActionListener implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent arg0) {
        System.out.println("MyActionListener: Action");
    }
    
}
/**
 *
 * @author sairu
 */
public class HelloMe implements ActionListener{
    public static void main(String[] args) {
        JFrame frmMain = new JFrame("Hello Me");
        frmMain.setSize(500, 300);
        frmMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JLabel lbYourName = new JLabel("Your Name");
        lbYourName.setSize(80,20);
        lbYourName.setLocation(5, 5);
        lbYourName.setBackground(Color.white);
        lbYourName.setOpaque(true);
        
        JTextField txtYourName = new JTextField();
        txtYourName.setSize(200, 20);
        txtYourName.setLocation(90, 5);
        
        JButton btnHello = new JButton("Hello");
        btnHello.setSize(80, 20);
        btnHello.setLocation(90, 40);
        
        MyActionListener myActionListener = new MyActionListener();
        btnHello.addActionListener(myActionListener);
        btnHello.addActionListener(new HelloMe());
        
        ActionListener actionListener = new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent arg0) {
                System.out.println("Anonymous Class: Action");
            }
            
        };
        btnHello.addActionListener(actionListener);
        
        JLabel lbHello = new JLabel("Hello...",JLabel.CENTER);
        lbHello.setSize(200, 20);
        lbHello.setLocation(90, 80);
        lbHello.setBackground(Color.white);
        lbHello.setOpaque(true);
        
        frmMain.setLayout(null);
        
        frmMain.add(lbYourName);
        frmMain.add(txtYourName);
        frmMain.add(btnHello);
        frmMain.add(lbHello);
        
        btnHello.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent arg0) {
                String name = txtYourName.getText();
                lbHello.setText("Hello "+name);
                        
            }
        
        });
        
        frmMain.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        System.out.println("HelloMe: Action");
    }
}
